const express = require('express');
const {engine} = require('express-handlebars');
const myconnection = require('express-myconnection');
const mysql2 = require('mysql2');
const session = require('express-session');
const bodyparser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const bodyParser = require('body-parser');

// Iniciar puerto y login
const app = express();
app.set('port', 4000);

app.set('views', __dirname + '/views');

app.engine('.hbs', engine({
    extname: '.hbs', 
}));

app.set('view engine','.hbs');

app.use(bodyParser.urlencoded({
    extended: true
}));



app.listen(app.get('port'), () => {
	console.log('Iniciando en puerto: ', app.get('port'));
});

